use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct UpcloudAccountsListRoot {
    pub accounts: UpcloudAccountListRoot,
}

#[derive(Deserialize, Debug)]
pub struct UpcloudAccountListRoot {
    pub account: Vec<UpcloudAccountsListItem>,
}

#[derive(Deserialize, Debug)]
pub struct UpcloudAccountsListItem {
    #[serde(rename = "type")]
    pub account_type: String,
    pub username: String,
    pub roles: UpcloudRoleRoot,
}

#[derive(Deserialize, Debug)]
pub struct UpcloudRoleRoot {
    pub role: Vec<String>,
}

#[derive(Deserialize, Debug)]
pub struct UpcloudAccountRoot {
    pub account: UpcloudAccount,
}

#[derive(Deserialize, Debug)]
pub struct UpcloudAccountResources {
    pub cores: f32,
    pub detached_floating_ips: Option<f32>,
    pub memory: f32,
    pub networks: u32,
    pub public_ipv4: u32,
    pub public_ipv6: u64,
    pub storage_hdd: f32,
    pub storage_maxiops: Option<f32>,
    pub storage_ssd: f32,
}

#[derive(Deserialize, Debug)]
pub struct UpcloudAccount {
    pub credits: f32,
    pub resource_limits: UpcloudAccountResources,
    pub username: String
}