use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct UpcloudServerTemplateListRoot {
    pub storages: UpcloudServerTemplateList,
}

#[derive(Deserialize, Debug)]
pub struct UpcloudServerTemplateList {
    pub storage: Vec<UpcloudServerTemplate>,
}

#[derive(Deserialize, Debug)]
pub struct UpcloudServerTemplate {
    pub access: String,
    pub license: f32,
    pub size: u32,
    pub state: String,
    pub template_type: String,
    pub title: String,
    #[serde(rename = "type")]
    pub ttype: String,
    pub uuid: String,
}
