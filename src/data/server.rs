use serde::{Deserialize, Serialize};

#[derive(Deserialize, Debug)]
pub struct UpcloudServerListRoot {
    pub servers: UpcloudServerList,
}

#[derive(Deserialize, Debug)]
pub struct UpcloudServerList {
    pub server: Vec<UpcloudServer>,
}

#[derive(Deserialize, Debug)]
pub struct UpcloudServerRoot {
    pub server: UpcloudServer,
}

#[derive(Deserialize, Debug)]
pub struct UpcloudServer {
    pub core_number: String,
    pub hostname: String,
    pub labels: UpcloudLabelList,
    pub license: u32,
    pub memory_amount: String,
    pub plan: String,
    pub plan_ipv4_bytes: String,
    pub plan_ipv6_bytes: String,
    pub state: String,
    pub tags: UpcloudTagList,
    pub title: String,
    pub uuid: String,
    pub zone: String,
}


#[derive(Deserialize, Serialize, Debug)]
pub struct UpcloudLabelList {
    pub label: Vec<UpcloudLabel>
}

#[derive(Deserialize, Serialize, Debug)]
pub struct UpcloudLabel {
    pub key: String,
    pub value: String,
}

#[derive(Deserialize, Debug)]
pub struct UpcloudTagList {
    pub tag: Vec<String>
}
