use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct UpcloudPlanListRoot {
    pub plans: UpcloudPlanList,
}

#[derive(Deserialize, Debug)]
pub struct UpcloudPlanList {
    pub plan: Vec<UpcloudPlan>,
}

#[derive(Deserialize, Debug)]
pub struct UpcloudPlan {
    pub core_number: u32,
    pub memory_amount: u32,
    pub name: String,
    pub public_traffic_out: u32,
    pub storage_size: u32,
    pub storage_tier: String,
}
