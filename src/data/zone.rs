use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct UpcloudZoneListRoot {
    pub zones: UpcloudZoneList,
}

#[derive(Deserialize, Debug)]
pub struct UpcloudZoneList {
    pub zone: Vec<UpcloudZone>,
}

#[derive(Deserialize, Debug)]
pub struct UpcloudZone {
    pub description: String,
    pub id: String,
    pub public: String,
}
