use std::collections::HashMap;

use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct UpcloudPricesListRoot {
    pub prices: UpcloudPricesZoneRoot,
}

#[derive(Deserialize, Debug)]
pub struct UpcloudPricesZoneRoot {
    pub zone: Vec<UpcloudPricesZone>,
}

#[derive(Deserialize, Debug)]
pub struct UpcloudPricesZone {
    pub name: String,
    pub firewall: UpcloudPrice,
    pub io_request_backup: UpcloudPrice,
    pub io_request_hdd: UpcloudPrice,
    pub io_request_maxiops: UpcloudPrice,
    pub ipv4_address: UpcloudPrice,
    pub ipv6_address: UpcloudPrice,
    pub public_ipv4_bandwidth_in: UpcloudPrice,
    pub public_ipv4_bandwidth_out: UpcloudPrice,
    pub public_ipv6_bandwidth_in: UpcloudPrice,
    pub public_ipv6_bandwidth_out: UpcloudPrice,
    pub server_core: UpcloudPrice,
    pub server_memory: UpcloudPrice,
    pub storage_backup: UpcloudPrice,
    pub storage_hdd: UpcloudPrice,
    pub storage_maxiops: UpcloudPrice,
    #[serde(flatten)]
    pub extra: HashMap<String, UpcloudPrice>,
}

#[derive(Deserialize, Debug)]
pub struct UpcloudPrice {
    /// The amount of objects the price applies to
    pub amount: f32,
    /// price in euro cent per hour. to get euros multiply with 100
    pub price: f32,
}
