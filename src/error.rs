use std::{fmt::Display, error::Error};

#[derive(Debug)]
pub enum UpcloudError {
    Reqwest(reqwest::Error),
    Api(String),
    Json(),
}

impl From<reqwest::Error> for UpcloudError {
    fn from(value: reqwest::Error) -> Self {
        UpcloudError::Reqwest(value)
    }
}

impl Display for UpcloudError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        match self {
            UpcloudError::Reqwest(err) => write!(f, "Request: {}", err),
            UpcloudError::Api(err) => write!(f, "Api: {}", err),
            UpcloudError::Json() => write!(f, "JSON"),
        }
    }
}

impl Error for UpcloudError {}
