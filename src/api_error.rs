use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct UpcloudApiErrorRoot {
    pub error: UpcloudApiError,
}

#[derive(Deserialize, Debug)]
pub struct UpcloudApiError {
    pub error_code: String,
    pub error_message: String,
}