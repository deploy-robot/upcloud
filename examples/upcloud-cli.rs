use std::env;
use upcloud_rs::{UpcloudApi, UpcloudError};

fn do_stuff(username: &str, password: &str) -> Result<(), UpcloudError> {
    let api = UpcloudApi::new(username, password);

    let account = api.get_account_info()?;
    println!("ACCOUNT: {:?}", account);

    let account_list = api.get_account_list()?;
    println!("ACCOUNT LIST: {:?}", account_list);

    let prices = api.get_prices()?;
    println!("PRICES: {:#?}", prices);

    let zones = api.get_zones()?;
    println!("ZONES: {:#?}", zones);

    let plans = api.get_plans()?;
    println!("PLANS: {:#?}", plans);

    let servers = api.get_servers()?;
    println!("SERVERS: {:#?}", servers);

    let templates = api.get_server_templates()?;
    println!("SERVER TEMPLATES: {:#?}", templates);

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 3 {
        println!("Call program with the following:");
        println!("{} UPCLOUD_USERNAME UPCLOUD_PASSWORD", args[0]);
        std::process::exit(1);
    }

    let result = do_stuff(&args[1], &args[2]);
    match result {
        Ok(_) => {
            println!("everything ok");
        }
        Err(e) => {
            println!("error: {}", e);
        }
    }
}
