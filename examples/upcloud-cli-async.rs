use std::env;
use upcloud_rs::{UpcloudApi, UpcloudError};

async fn do_stuff(username: &str, password: &str) -> Result<(), UpcloudError> {
    let api = UpcloudApi::new(username, password);

    let account = api.get_account_info_async().await?;
    println!("ACCOUNT: {:?}", account);

    let account_list = api.get_account_list_async().await?;
    println!("ACCOUNT LIST: {:?}", account_list);

    let prices = api.get_prices_async().await?;
    println!("PRICES: {:#?}", prices);

    let zones = api.get_zones_async().await?;
    println!("ZONES: {:#?}", zones);

    let plans = api.get_plans_async().await?;
    println!("PLANS: {:#?}", plans);

    let servers = api.get_servers_async().await?;
    println!("SERVERS: {:#?}", servers);

    let templates = api.get_server_templates_async().await?;
    println!("SERVER TEMPLATES: {:#?}", templates);

    Ok(())
}

#[async_std::main]
async fn main() -> Result<(), UpcloudError> {
    let args: Vec<String> = env::args().collect();
    if args.len() != 3 {
        println!("Call program with the following:");
        println!("{} UPCLOUD_USERNAME UPCLOUD_PASSWORD", args[0]);
        std::process::exit(1);
    }

    let result = do_stuff(&args[1], &args[2]).await;
    match result {
        Ok(_) => {
            println!("everything ok");
        }
        Err(e) => {
            println!("error: {}", e);
        }
    }
    Ok(())
}
